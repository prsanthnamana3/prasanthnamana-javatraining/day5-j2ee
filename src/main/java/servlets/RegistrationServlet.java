package servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.catalina.connector.Request;

import DAO.UserDAO;

/**
 * Servlet implementation class RegistrationServlet
 */
@WebServlet("/Register")
public class RegistrationServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public RegistrationServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String userName =  request.getParameter("userName");
		String firstName =  request.getParameter("fName");
		String lastName = request.getParameter("lName");
		String email = request.getParameter("email");
		String password =  request.getParameter("passwd");
		
		int res = new UserDAO().registerUser(userName,firstName,lastName,email,password);
		if(res ==1) {
			request.setAttribute("name",firstName+" "+lastName);
			request.getRequestDispatcher("Success.jsp").forward(request, response);
		}
		else {
			request.setAttribute("message", "Error while Registering User");
			request.getRequestDispatcher("Error.jsp").forward(request, response);
		}
	}

}
