package services;

import POJO.User;

public interface UserService {
	public User getUser(String userName);
	public User getUserFromEmail(String email);
	public int registerUser(User user);
}
