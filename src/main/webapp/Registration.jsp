<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Registration</title>
</head>
<body>
	<p>Registration Here:</p>
	<table>
		<form action="./Register" method="post">
			<tr>
				<td>UserName :</td>
				<td><input name="userName" placeholder="Enter User Name"
					type="text"> <br></td>
			</tr>
			<tr>
				<td>Password :</td>
				<td><input name="passwd" type="password"
					placeholder="Enter your password"><br></td>
			</tr>
			<tr>
				<td>First Name :</td>
				<td><input name="fName" type="text"
					placeholder="Enter your First Name"><br></td>
			</tr>
			<tr>
				<td>Last Name :</td>
				<td><input name="lName" type="text"
					placeholder="Enter your Last Name"><br></td>
			</tr>
			<tr>
				<td>Email :</td>
				<td><input name="email" type="text"
					placeholder="Enter your Email"><br></td>
			</tr>
			<tr>
				<td><input type="submit" value="Register"></td>
				<td><a href="./login.jsp"> Click here to Login</a></td>
			</tr>

		</form>
	</table>
</body>
</html>