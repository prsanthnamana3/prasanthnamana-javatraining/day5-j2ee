package DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import POJO.User;
import database.DBConnection;
import services.UserService;

public class UserDAO implements UserService {
	private static Connection conn  = null;
	@Override
	public User getUser(String userName) {
		User user = null;
		try {
			String getUser = "select * from user_details where user_name = ?";
			getConnection();
			PreparedStatement ps = conn.prepareStatement(getUser);
			ps.setString(1, userName);
			ResultSet rs = ps.executeQuery();
			if(rs.next())
				user = new User(rs.getString("user_name"),
						rs.getString("first_name"),rs.getString("last_name"),rs.getString("email"), rs.getString("password"));
		}
		catch(Exception e) {
			System.out.println("Error while fetching user details for userName "+userName);
		}
		return user;
	}
	
	public User getUserFromEmail(String email) {
		User user = null;
		try {
			String getUser = "select * from user_details where email = ?";
			getConnection();
			PreparedStatement ps = conn.prepareStatement(getUser);
			ps.setString(1, email);
			ResultSet rs = ps.executeQuery();
			if(rs.next())
				user = new User(rs.getString("user_name"),
						rs.getString("first_name"),rs.getString("last_name"),rs.getString("email"), rs.getString("password"));
		}
		catch(Exception e) {
			System.out.println("Error while fetching user details for email "+email);
		}
		return user;
	}

	@Override
	public int registerUser(User user) {
		String insertUser = "insert into user_details (?,?,?,?)";
		int res =0;
		try{
			getConnection();
			PreparedStatement ps = conn.prepareStatement(insertUser);
			ps.setString(1, user.getUserName());
			ps.setString(2, user.getFirstName());
			ps.setString(3, user.getLastName());
			ps.setString(4, user.getEmail());
			res = ps.executeUpdate();
		}
		catch(Exception e) {
			System.out.println("Error while inserting user");
		}
		return res;
	}
	
	public int registerUser(String userName, String fName, String Lname, String email, String password) {
		String insertUser = "insert into user_details  values (?,?,?,?,?)";
		int res =0;
		try{
			getConnection();
			PreparedStatement ps = conn.prepareStatement(insertUser);
			ps.setString(1, userName);
			ps.setString(2, fName);
			ps.setString(3, Lname);
			ps.setString(4, email);
			ps.setString(5, password);
			res = ps.executeUpdate();
		}
		catch(Exception e) {
			System.out.println("Error while inserting user");
		}
		return res;
	}
	
	private void getConnection() {
		if(conn ==null) {
			conn = new DBConnection().getConnection();
		}
		
	}

}
