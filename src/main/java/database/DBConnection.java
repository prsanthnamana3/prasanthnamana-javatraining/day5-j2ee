package database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnection {
	public static final String url = "jdbc:mysql://localhost:3306/prasanth";
	public static final String userName = "root";
	public static final String password = "root";
	
	public Connection getConnection() {
		Connection conn = null;
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			conn = DriverManager.getConnection(url, userName, password);
		} catch (ClassNotFoundException e) {
			System.out.println("Sql class not found");
			e.printStackTrace();
		} catch (SQLException e) {
			System.out.println("Unable to get connection");
			e.printStackTrace();
		}
		return conn;
	}
	
}
