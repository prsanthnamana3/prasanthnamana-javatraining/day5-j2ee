package servlets;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;

import DAO.UserDAO;
import POJO.User;
import services.UserService;

/**
 * Servlet Filter implementation class AuthFilter
 */
@WebFilter(
		urlPatterns = { "/Login" }, 
		servletNames = { 
				"LoginServlet"
		})
public class LoginFilter implements Filter {

    /**
     * Default constructor. 
     */
    public LoginFilter() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		request.setAttribute("process", "Login");
		String userName = request.getParameter("userName");
		String passwd = request.getParameter("passwd");
		User user = new UserDAO().getUser(userName);
		if(user ==null || !user.getPassword().equals(passwd)) {
			request.setAttribute("message", "User doesn't exist or worng credentials");
			request.getRequestDispatcher("Error.jsp").forward(request, response);
			return;
		}
		request.setAttribute("details", user);
		
		chain.doFilter(request, response);
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}

}
