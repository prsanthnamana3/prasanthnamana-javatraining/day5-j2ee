package servlets;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;

import DAO.UserDAO;
import POJO.User;

/**
 * Servlet Filter implementation class RegistrationFilter
 */
@WebFilter(urlPatterns = { "/Register" }, servletNames = { "RegistrationServlet" })
public class RegistrationFilter implements Filter {

    /**
     * Default constructor. 
     */
    public RegistrationFilter() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		String userName = request.getParameter("userName");
		request.setAttribute("process", "Registration");
		String email = request.getParameter("email");
		String passwd = request.getParameter("passwd");
		User user = new UserDAO().getUser(userName);
		if(user !=null ) {
			request.setAttribute("message", "User Name already Taken");
			request.getRequestDispatcher("Error.jsp").forward(request, response);
		}
		
		user = new UserDAO().getUser(userName);
		if(user !=null ) {
			request.setAttribute("message", "Email already Exists");
			request.getRequestDispatcher("Error.jsp").forward(request, response);
			return;
		}
		chain.doFilter(request, response);
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}

}
